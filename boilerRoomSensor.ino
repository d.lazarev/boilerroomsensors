//BoilerRoomSensor
#include <driver/adc.h>
#include <SPI.h>
#include <Wire.h>
#include <WiFi.h>
#include <Adafruit_BME280.h>
#include <WiFiUdp.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <EEPROM.h>
#include <ESPmDNS.h>
#include <WebServer.h>

#define ledR 17
#define ledG 16
#define ledB 18

#define EEPROM_SIZE 64

#define ledPin 4
#define SDA 5
#define SCL 4
#define OLED_ADDR 0x3C

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

WiFiUDP udp;
Adafruit_BME280 bme; // I2C
WebServer httpd(80);

const char* ssid = "BOSON2.4"; //
const char* passwd = "kartoshka";
const byte influxdb_host[] = {138, 201, 158, 136}; // the IP address of your InfluxDB host
const int influxdb_port = 8089; // UDP port of your InfluxDB host
float temperature, pressure, humidity;
int adc_raw;

int rgb_lev1 = 300;
int rgb_lev2 = 500;

unsigned long nowTime;

void read_sensors(void);
void getWeather(void);
void getMQ_5(void);
int read_MQ_5_level(void);
void OLED_init();
void info_display(void);
void setAlarmLED(void);
void checkWebClient();
void sendHelloPage();
void sendErrorPage(WiFiClient *client);
void sendInfoPage(WiFiClient *client); 
void save_setup(String, String); 
void parse_params(int, int);

const char page_head[] PROGMEM = "\
<head>\
	<title>Boiler room sensors device</title>\
	<style>\ 
		body {\
			background-color: #D0D0F0;\
		}\
		input[type=number] {\
			width: 60px;\
		}\
	</style>\
</head>";

//---------------------

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(ledR, OUTPUT);
  pinMode(ledG, OUTPUT);
  pinMode(ledB, OUTPUT);

	Serial.begin(115200);
  delay(100);
  
	EEPROM.begin(EEPROM_SIZE);
	int saved_lev1 = EEPROM.readInt(0);
	int saved_lev2 = EEPROM.readInt(sizeof(int));
	Serial.print("EEPROM lev1=");
	Serial.println(saved_lev1);
	Serial.print("EEPROM lev2=");
	Serial.println(saved_lev2);

	parse_params(saved_lev1, saved_lev2);

  //ADC1 config
  adc1_config_width(ADC_WIDTH_BIT_12);
  adc1_config_channel_atten(ADC1_CHANNEL_7,ADC_ATTEN_DB_11);
  
  WiFi.begin(ssid, passwd);
  for (int i = 0; i < 20 && (WiFi.status() != WL_CONNECTED); i++) {
    delay(1500);
    Serial.print(".");
  }
  
  if (WiFi.status() != WL_CONNECTED) {
    Serial.println();
    Serial.println("WiFi is NOT connected. Spleeping 10 min.");
    esp_sleep_enable_timer_wakeup(600e6);
   
    for(int i=0; i<8; i++) {
      digitalWrite(ledPin, ! digitalRead(ledPin));
      delay(350);
    } 
    
    Serial.flush();

    esp_deep_sleep_start();
    
  }
 
	digitalWrite(ledB, HIGH);

  digitalWrite(ledPin, HIGH);
  Serial.println();
  Serial.println("WiFi connected.");

  // Print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
  
  Serial.println(WiFi.localIP());

  if (!bme.begin(0x76)) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1) {
      digitalWrite(ledPin, ! digitalRead(ledPin));
      delay(300);
    }
  }  
  OLED_init();
	digitalWrite(ledB, LOW);
	digitalWrite(ledG, HIGH);

	httpd.begin();
	MDNS.begin("brs");
	MDNS.addService("http", "tcp", 80);

	httpd.on("/", sendHelloPage);

	nowTime = millis();
}

//---------------------

void loop() {

	httpd.handleClient();

	if (millis() - nowTime > 5000) { 
		read_sensors();
  	info_display();
		setAlarmLED();
		Serial.print("rgb_lev1: ");
		Serial.println(rgb_lev1);
		Serial.print("rgb_lev2: ");
		Serial.println(rgb_lev2);
		nowTime = millis();
	}
  delay(100);
}

//---------------------

void setAlarmLED() {
	digitalWrite(ledR, LOW);
	digitalWrite(ledG, LOW);
	digitalWrite(ledB, LOW);

	if (adc_raw <= rgb_lev1) digitalWrite(ledG, 64);
	if (adc_raw > rgb_lev1 && adc_raw < rgb_lev2) {
		digitalWrite(ledG, 64);
		digitalWrite(ledB, 64);
	}
	if (adc_raw >= rgb_lev2) digitalWrite(ledR, 64);
}

//---------------------

void info_display() {
  display.clearDisplay();
  display.setCursor(0,0); 
  
	display.setTextSize(2);             // Draw 2X-scale text
  display.setTextColor(SSD1306_WHITE);
  display.print("MQ5: ");
  display.println(adc_raw);

  display.setTextSize(1);             // Draw 1X-scale text
  display.print("Pressure: ");
  display.print(pressure, 0);
  display.println(" mmHg");

	display.print("Temp:     ");
  display.print(temperature, 0);
  display.println(" C");

	display.print("Humidity: ");
  display.print(humidity, 1);
  display.println("%");
  
	display.display();
}

//---------------------

void OLED_init() {
  if(!display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR)) { // Address 0x3D for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }
  
  display.display();
  delay(2000); // Pause for 2 seconds

  // Clear the buffer
  display.clearDisplay();

  display.setTextSize(1);             // Draw 2X-scale text
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(35,35); 
  display.println("particles.su");
  // Show the display buffer on the screen. You MUST call display() after
  // drawing commands to make them visible on screen!
  display.display();
 
}

//---------------------

void getWeather() {
  temperature = bme.readTemperature();
  pressure = bme.readPressure() * 7.5 / 1000.0;
  humidity = bme.readHumidity();
}

//---------------------

void getMQ_5(void) {
  adc_raw = read_MQ_5_level();
}

//---------------------

void read_sensors() {

  String udp_data;

  getWeather();
  getMQ_5();
  
  Serial.println();
  Serial.print("Temperature = ");
  Serial.println(temperature);

  Serial.print("Pressure = ");
  Serial.println(pressure);

  Serial.print("Humidity = ");
  Serial.println(humidity);

  Serial.print("MQ-5 = ");
  Serial.println(adc_raw);

  udp_data = String("BME280,device_id=003 temperature=" + String(temperature));
  udp_data += String(",humidity=" + String(humidity));
  udp_data += String(",pressure=" + String(pressure));
  udp_data += String(",MQ-5=" + String(adc_raw));
  Serial.println("Sending UDP packet...");
  Serial.println(udp_data);
  udp.beginPacket(influxdb_host, influxdb_port);
  udp.print(udp_data);
  udp.endPacket();
}

//---------------------

int read_MQ_5_level() {
  int val = 0;
  for(int i=0; i<10; i++) {
    val += adc1_get_raw(ADC1_CHANNEL_7);
    delay(5);
  }
  return val/10;
}

//---------------------

void checkWebClient() {
/*
	WiFiClient client = httpd.available();
	if (!client) return;

	digitalWrite(ledPin, 0); // Turn off led on client processing

	// Wait for data from client to become available
	while(client.connected() && !client.available()) delay(1);

	Serial.print("rgb_lev1=");
	Serial.println(httpd.arg("rgb_lev1"));

	// Read the first line of HTTP request
	String req = client.readStringUntil('\r');

	// First line of HTTP request looks like "GET /path HTTP/1.1"
	// Retrieve the "/path" part by finding the spaces
	int addr_start = req.indexOf(' ');
	int addr_end = req.indexOf(' ', addr_start + 1);
	if (addr_start == -1 || addr_end == -1) return;
	req = req.substring(addr_start + 1, addr_end);

	if (req == "/") 
		sendHelloPage(&client);

	else if (req == "/info") 
		sendInfoPage(&client);
											
	else 
		sendErrorPage(&client);
																												    
	client.stop();
																																    
	digitalWrite(ledPin, 1); // Turn on led on client waiting
*/
}

//---------------------

void sendHelloPage() {
	IPAddress ip = WiFi.localIP();
	String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
	String s = "<!DOCTYPE HTML>\n<html lang=ru>\n";

	s += page_head;

	s += "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n";
	s += "<h1>Hello from brs.local at " + ipStr + "</h1><br><hr>\n";
	s += String("device_id=003 <br>\n");
	s += String("temperature " + String(temperature)) + "<br>\n";
	s += String("humidity " + String(humidity)) + "<br>\n";
	s += String("pressure " + String(pressure)) + "<br>\n";
	s += String("MQ-5 " + String(adc_raw)) + "<br><hr>\n";

	s += "<h2>Настройка значений пороговых границ для датчика MQ-5</h2>";
	s += "<form action=\"/\" method=\"POST\">\n";
	s += "<table>";
	s += String("<tr><td><label for=\"rgb_lev1\">Нижняя граница:\t</label></td><td><input type=\"number\" id=\"rgb_lev1\" name=\"rgb_lev1\" value=\"") + String(rgb_lev1) + "\"></td></tr>\n";
	s += String("<tr><td><label for=\"rgb_lev2\">Верхняя граница:\t</label></td><td><input type=\"number\" id=\"rgb_lev2\" name=\"rgb_lev2\" value=\"") + String(rgb_lev2) + "\"></td></tr>\n";
	s += String("<tr><td><input type=\"submit\" value=\"Установить новые значения\"></td></tr>");
	s += "</table>";
	s += "</form>\n";
	s += "</html>\n";
	httpd.send(200, "text/html",s);
	save_setup(httpd.arg("rgb_lev1"), httpd.arg("rgb_lev2"));
}

//---------------------

void save_setup(String arg1, String arg2) {
	if (arg1 == "" || arg2 == "") return;
	
	int new_lev1 = arg1.toInt();
	int new_lev2 = arg2.toInt();
	parse_params(new_lev1, new_lev2);
}

//---------------------

void sendErrorPage(WiFiClient *client) {
	String s = "HTTP/1.1 404 Not Found\r\n\r\n";
	client->print(s);
}


void sendInfoPage(WiFiClient *client) {

}

void parse_params(int lev1, int lev2) {
	if (lev1 <= 0 || lev1 > 4096) lev1 = 300;
	if (lev2 <= 0 || lev2 > 4096 || lev2 <= lev1) lev2 = 500;

	EEPROM.writeInt(0, lev1);
	EEPROM.writeInt(sizeof(int), lev2);
	EEPROM.commit();

	rgb_lev1 = lev1;
	rgb_lev2 = lev2;
}
